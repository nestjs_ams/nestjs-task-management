import * as bcryptjs from 'bcryptjs';

import { User } from './user.entity';

describe('UserEntity', () => {
  describe('validatePassword', () => {
    let user: User;

    beforeEach(() => {
      user = new User({
        username: 'test',
        password: 'testpw',
      });

      (bcryptjs.compare as any) = jest.fn();
    });

    it('returns true as password is valid', async () => {
      (bcryptjs.compare as any).mockReturnValue(true);
      expect(bcryptjs.compare).not.toHaveBeenCalled();
      const result = await user.validatePassword('1234');
      expect(bcryptjs.compare).toHaveBeenCalledWith('1234', user.password);
      expect(result).toEqual(true);
    });

    it('returns false as password is invalid', async () => {
      (bcryptjs.compare as any).mockReturnValue(false);
      expect(bcryptjs.compare).not.toHaveBeenCalled();
      const result = await user.validatePassword('12345');
      expect(bcryptjs.compare).toHaveBeenCalledWith('12345', user.password);
      expect(result).toEqual(false);
    });
  });
});
