import {
  InternalServerErrorException,
  ConflictException,
  Logger,
} from '@nestjs/common';
import { Repository, EntityRepository } from 'typeorm';

import * as bcryptjs from 'bcryptjs';

import { User } from './user.entity';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  private logger: Logger = new Logger('UserRepository');

  async signUp(authCredentialsDto: AuthCredentialsDto): Promise<void> {
    const user = this.create({
      username: authCredentialsDto.username,
      password: await this.hashPassword(
        authCredentialsDto.password,
        await bcryptjs.genSalt(),
      ),
    });

    this.logger.debug(
      `Created user with payload ${JSON.stringify(
        authCredentialsDto,
      )}. User: ${JSON.stringify(user)}`,
    );

    try {
      await user.save();
    } catch (e) {
      if (e.code === '23505') {
        this.logger.error(
          `Failed to sign up user "${
            user.username
          }" username already exists. DTO: ${JSON.stringify(
            authCredentialsDto,
          )}`,
          e.stack,
        );
        // duplicate username
        throw new ConflictException('Username already exists');
      } else {
        this.logger.error(
          `Failed to sign up user "${user.username}". DTO: ${JSON.stringify(
            authCredentialsDto,
          )}`,
          e.stack,
        );
        throw new InternalServerErrorException();
      }
    }
  }

  async validateUserPassword({
    username,
    password,
  }: AuthCredentialsDto): Promise<string> {
    const user = await this.findOne({ username });

    if (user && (await user.validatePassword(password))) {
      return user.username;
    }

    return null;
  }

  private async hashPassword(password: string, salt: string): Promise<string> {
    return bcryptjs.hash(password, salt);
  }
}
